package IntegrationTests.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.util.UUID;

public class CreateIssue {
    private final WebDriver driver;
    public String issueId;

    By summary = By.id("summary");
    By description = By.id("description");
    By create = By.id("create-issue-submit");
    By update = By.id("edit-issue-submit");

    public CreateIssue(WebDriver driver){
        this.driver = driver;
    }

    public HomePage create(){
        driver.findElement(create).click();
        return new HomePage(driver);
    }

    public void update(){
        driver.findElement(update).click();
    }

    public String firstInfo(){
        issueId = "Test Summary - " + UUID.randomUUID();
        driver.findElement(summary).sendKeys(issueId);
        driver.findElement(description).sendKeys("This is the first test description.");
        create();
        return issueId;
    }

    public String secondInfo(){
        issueId = "Test Summary - " + UUID.randomUUID();
        waitForElement(summary);
        driver.findElement(summary).sendKeys(issueId);
        update();
        return issueId;
    }

    public String getIssueId(){
        return issueId;
    }

    public void waitForElement(By locator){
        try{
            driver.findElement(locator);
        } catch (NoSuchElementException e) {
            waitForElement(locator);
        }
    }

}
