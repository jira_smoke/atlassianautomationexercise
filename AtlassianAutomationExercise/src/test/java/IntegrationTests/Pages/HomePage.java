package IntegrationTests.Pages;

import org.openqa.selenium.*;

public class HomePage {
    private final WebDriver driver;
    private final String baseUrl = "https://jira.atlassian.com/browse/TST";

    By createIssue = By.id("create_link");
    By issueSuccess = By.className("issue-created-key");
    By findLink = By.id("find_link");
    By searchIssues = By.id("issues_new_search_link_lnk");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage navigateToHome(){
        driver.navigate().to(baseUrl);
        return this;
    }

    public CreateIssue createIssue(){
        driver.findElement(createIssue).click();
        return new CreateIssue(driver);
    }

    public SearchPage search(){
        driver.findElement(findLink).click();
        waitForElement(searchIssues);
        driver.findElement(searchIssues).click();

        return new SearchPage(driver);
    }

    public boolean isIssueCreated(){
        waitForElement(issueSuccess);
        return driver.findElement(issueSuccess).isDisplayed();
    }

    public void waitForElement(By locator){
        try{
            driver.findElement(locator);
        } catch (NoSuchElementException e) {
            waitForElement(locator);
        }
    }
}
