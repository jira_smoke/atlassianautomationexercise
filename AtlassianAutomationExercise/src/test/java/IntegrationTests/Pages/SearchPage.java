package IntegrationTests.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchPage {
    private final WebDriver driver;

    public SearchPage(WebDriver driver){
        this.driver = driver;
    }

    By search = By.id("searcher-query");
    By link = By.cssSelector(".issue-link-summary");
    By header = By.id("summary-val");
    By updateConfirm = By.cssSelector("div.aui-message.success.closeable");

    public SearchPage searchIssue(String issue){
        waitForElement(search);
        driver.findElement(search).sendKeys('"' + issue + '"' + Keys.ENTER);

        List<WebElement> linkList;
        do{
            linkList = driver.findElements(link);
        } while (linkList.size() > 1);
        return this;
    }

    public boolean isLink(String issueId){
        String linkText = driver.findElement(link).getText();
        return issueId.equals(linkText);
    }

    public CreateIssue updateHeader() {
        waitForElement(header);
        driver.findElement(header).sendKeys("something");
        return new CreateIssue(driver);
    }

    public boolean isUpdated() {
        waitForElement(updateConfirm);
        return driver.findElement(updateConfirm).isDisplayed();
    }

    public void waitForElement(By locator){
        try{
            driver.findElement(locator);
        } catch (NoSuchElementException e) {
            waitForElement(locator);
        }
    }

}
