package IntegrationTests.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginPage {
    private final WebDriver driver;
    private final String loginUrl = "https://id.atlassian.com/id/login.action";

    By usernameLocator = By.id("username");
    By passwordLocator = By.id("password");
    By loginButtonLocator = By.id("loginbutton");

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public LoginPage login(){
        navigateToLogin();

        driver.findElement(usernameLocator).sendKeys("jirasmoke@gmail.com");
        driver.findElement(passwordLocator).sendKeys("smoketest");
        driver.findElement(loginButtonLocator).click();

        return this;

    }

    public LoginPage navigateToLogin(){
        driver.navigate().to(loginUrl);
        return this;
    }
}
