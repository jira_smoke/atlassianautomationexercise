package IntegrationTests.Tests;

import IntegrationTests.Pages.HomePage;
import IntegrationTests.Pages.SearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import IntegrationTests.Pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class SmokeTest {
    public WebDriver driver;
    public String issueId;

    @BeforeClass
    public void testSignOn(){
       driver = new FirefoxDriver();

       new LoginPage(driver).login();
    }

    @AfterClass
    public void stopWebDriver(){
        driver.close();
    }

    @Test (timeOut = 30000)
    public void createIssue(){
        HomePage homePage = new HomePage(driver);

        issueId = homePage.navigateToHome()
                .createIssue()
                .firstInfo();

        assertTrue(homePage.isIssueCreated(), "Issue was not successfully created");
    }

    @Test (dependsOnMethods = "createIssue", timeOut = 30000)
    public void searchIssue(){
        HomePage homePage = new HomePage(driver);

        SearchPage searchPage = homePage.search();
        searchPage.searchIssue(issueId);

        assertTrue(searchPage.isLink(issueId));
    }

    @Test (dependsOnMethods = "searchIssue", timeOut = 30000)
    public void updateIssue(){
        SearchPage searchPage = new SearchPage(driver);
        searchPage.updateHeader().secondInfo();

        assertTrue(searchPage.isUpdated());
    }

}
