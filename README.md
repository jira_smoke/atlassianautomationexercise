**NOTE:** The test suite uses Maven to resolve it's dependencies so it will be necessary to run:

```
    mvn clean clean install
```

Then to run the tests, use the Maven profile named 'run':

```
    mvn clean verify -P run
```